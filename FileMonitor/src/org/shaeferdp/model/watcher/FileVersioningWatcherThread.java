package org.shaeferdp.model.watcher;

import java.io.File;
import java.util.Date;
import java.util.Map;

import org.joda.time.DateTime;
import org.shaeferdp.model.NotificationMethod;
import org.shaeferdp.model.enums.MonitorType;


public class FileVersioningWatcherThread extends DirectoryWatcherThread
{

   public FileVersioningWatcherThread(File file, NotificationMethod notificationMethod, Map<File, WatcherThread> mapOfWatchedFiles, File dest)
   {
      super(file, notificationMethod, mapOfWatchedFiles);
      this.monitorType = MonitorType.FILE_VERSIONING;
      this.watchSingleFile = true;
      this.versionedFileDestinationDir = dest;
   }

   public FileVersioningWatcherThread(File file, NotificationMethod notificationMethod, Map<File, WatcherThread> mapOfWatchedFiles, File dest, Date endWatch)
   {
      this(file, notificationMethod, mapOfWatchedFiles, dest);
      if (endWatch != null)
         this.endWatch = new DateTime(endWatch);
   }
   
}
