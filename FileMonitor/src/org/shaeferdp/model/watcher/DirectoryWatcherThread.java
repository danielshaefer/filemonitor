package org.shaeferdp.model.watcher;

import java.io.File;
import java.util.Date;
import java.util.Map;

import org.joda.time.DateTime;
import org.shaeferdp.model.NotificationMethod;
import org.shaeferdp.model.enums.MonitorType;

public class DirectoryWatcherThread extends WatcherThread
{
   public DirectoryWatcherThread(File file, NotificationMethod notificationMethod, Map<File, WatcherThread> mapOfWatchedFiles)
   {
      this.monitorType = MonitorType.DIRECTORY;
      this.file = file;
      this.notificationMethod = notificationMethod;
      this.mapOfWatchedFiles = mapOfWatchedFiles;
   }

   public DirectoryWatcherThread(File file, NotificationMethod notificationMethod, Map<File, WatcherThread> mapOfWatchedFiles, Date endWatch)
   {
      this(file, notificationMethod, mapOfWatchedFiles);
      if (endWatch != null)
         this.endWatch = new DateTime(endWatch);
   }
}
