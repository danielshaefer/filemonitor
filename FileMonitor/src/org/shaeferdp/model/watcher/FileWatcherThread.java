package org.shaeferdp.model.watcher;

import java.io.File;
import java.util.Date;
import java.util.Map;

import org.joda.time.DateTime;
import org.shaeferdp.model.NotificationMethod;
import org.shaeferdp.model.enums.MonitorType;

public class FileWatcherThread extends DirectoryWatcherThread
{

   public FileWatcherThread(File file, NotificationMethod notificationMethod, Map<File, WatcherThread> mapOfWatchedFiles)
   {
      super(file, notificationMethod, mapOfWatchedFiles);
      this.monitorType = MonitorType.FILE;
      this.watchSingleFile = true;
   }

   public FileWatcherThread(File file, NotificationMethod notificationMethod, Map<File, WatcherThread> mapOfWatchedFiles, Date endWatch)
   {
      this(file, notificationMethod, mapOfWatchedFiles);
      if (endWatch != null)
         this.endWatch = new DateTime(this.endWatch);
   }
   
}
