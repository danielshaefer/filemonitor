package org.shaeferdp.model.watcher;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.shaeferdp.model.FileCompare;
import org.shaeferdp.model.NotificationMethod;
import org.shaeferdp.model.enums.MonitorType;

public abstract class WatcherThread extends Thread implements Closeable
{
   private static Logger logger = Logger.getLogger(WatcherThread.class.getName());
   protected MonitorType monitorType;
   protected File file;
   protected int pollInterval = DateTimeConstants.MILLIS_PER_DAY;
   protected NotificationMethod notificationMethod;
   protected Map<File, WatcherThread> mapOfWatchedFiles;
   protected volatile boolean closed; 
   private WatchService watcher;
   protected boolean watchSingleFile;
   protected DateTime endWatch;
   protected File versionedFileDestinationDir;
   protected Integer version;
   
   @Override
   public void run()
   {
      try
      {
         watcher = FileSystems.getDefault().newWatchService();
         Path path = file.toPath();
         if (!file.isDirectory() && monitorType == MonitorType.DIRECTORY)
         {
            path = new File(file.getParent()).toPath();
            file = file.getParentFile();
         }
         else if (!file.isDirectory())
         {
            path = new File(file.getParent()).toPath();
         }
         logger.info("Started " + monitorType + " watcher using path: " + file);
         mapOfWatchedFiles.put(file, this);
         WatchKey key = path.register(watcher, 
                     StandardWatchEventKinds.ENTRY_CREATE,
                     StandardWatchEventKinds.ENTRY_DELETE,
                     StandardWatchEventKinds.ENTRY_MODIFY);
         while(!closed && !watchIsOver())
         {
            try
            {
               key = watcher.take();
            }
            catch (InterruptedException e)
            {
               e.printStackTrace();
            }
            catch (ClosedWatchServiceException e)
            {
               if (closed)
               return;
               else
                  throw new RuntimeException("Seemed to close unexpectedly", e);
            }
            if (!mapOfWatchedFiles.containsKey(file) || watchIsOver())
            {
               break;
            }
            List<WatchEvent<?>> events = key.pollEvents();
            for(WatchEvent<?> event: events)
            {
               WatchEvent.Kind<?> kind = event.kind();
               WatchEvent<Path> ev = (WatchEvent<Path>)event;
               Path filename = ev.context();
               File contextFile = new File(path + "\\" + filename);
               if (watchSingleFile && filename.toString().equals(file.getName()))
               {
                  if (monitorType == MonitorType.FILE_VERSIONING)
                  {
                     File verInfoFile = new File(versionedFileDestinationDir, ".fminfo");
                     Map<String, Integer> versionsByFile = buildVersionMapFromInfoFile(verInfoFile);
                     if (verInfoFile.exists())
                     {
                        version = versionsByFile.get(file.getAbsoluteFile().toString());
                        if (version == null)
                           version = 0;
                     }
                     File prevFile = getPreviousVersion(contextFile, version);
                     if (version == 0 || !FileCompare.areIdentical(contextFile, prevFile))
                     {
                        version++;
                        versionsByFile.put(file.getAbsoluteFile().toString(), version);
                        FileUtils.writeStringToFile(verInfoFile, convertVersionsMapToString(versionsByFile));
                        FileUtils.copyFile(contextFile, getPreviousVersion(contextFile, version));
                     }
                     else
                     {
                        logger.warning("Files were identical...did not version: " + contextFile.getAbsolutePath());
                     }
                  }
                  notificationMethod.doNotify(contextFile, kind, monitorType);
               }
               else if (!watchSingleFile)
                  notificationMethod.doNotify(contextFile, kind, monitorType);
            }
            boolean valid = key.reset();
            if (!valid)
            {
               System.out.println("Key is no longer valid meaning the directory is inaccessible.");
               break;
            }

         }
         logger.info(monitorType + " watcher has finished watching.");
      }
      catch (IOException e)
      {
         e.printStackTrace();
         throw new RuntimeException("IO Exception occurred when executing WatcherThread.run()", e);
      }
      finally
      {
         mapOfWatchedFiles.remove(file);
      }
   }

   protected String convertVersionsMapToString(Map<String, Integer> versionsByFile)
   {
      StringBuffer sb = new StringBuffer();
      String lineBreak = "";
      for (String key : versionsByFile.keySet())
      {
         sb.append(lineBreak).append(key + "," + versionsByFile.get(key));
         lineBreak = "\n";
      }
      return sb.toString();
   }

   public static Map<String, Integer> buildVersionMapFromInfoFile(File verInfoFile)
   {
      Map<String, Integer> map = new HashMap<String, Integer>();
      if (verInfoFile.exists())
      {
         try
         {
         
         List<String> lines = FileUtils.readLines(verInfoFile);
         for (String line : lines)
         {
            String filePath = StringUtils.substringBefore(line, ",");
            int version = NumberUtils.toInt(StringUtils.substringAfter(line, ","));
            map.put(filePath, version);
         }
         return map;
         }
         catch(IOException ex)
         {
            throw new RuntimeException("Could not read info file.", ex);
         }
      }
      return map;
   }
   
   protected File getPreviousVersion(File contextFile, int version)
   {
      String nameWithoutSuffix = StringUtils.substringBeforeLast(contextFile.getName(), ".");
      String suffix = StringUtils.substringAfterLast(contextFile.getName(), ".");
      if (StringUtils.isNotBlank(suffix))
         suffix = "." + suffix;
      return new File(versionedFileDestinationDir, nameWithoutSuffix + "_ver_" + version + suffix);
   }

   protected boolean watchIsOver()
   {
      if (new DateTime().isAfter(endWatch))
      {
         logger.info(monitorType + " watcher expired at: " + endWatch.toString("MM/dd/yyyy hh:mm:ss a"));
         this.closeQuietly();
         return true;
      }
      return false;
   }

   public void closeQuietly()
   {
      try
      {
         this.close();
      }
      catch (IOException e)
      {
         e.printStackTrace();
         throw new RuntimeException();
      }
   }
   
   @Override
   public void close() throws IOException
   {
      watcher.close();
      closed = true;
   }

   public MonitorType getMonitorType()
   {
      return monitorType;
   }
   
   /**
    * Sets a specified time to stop watching the given directory or file.
    * Setting an end time which as already expired will stop the watcher immediately.
    * Setting an end time when one was already specified upon construction will override the original.
    * @param endWatch
    */
   public void stopWatchingAt(Date endWatch)
   {
      if (endWatch != null)
         this.endWatch = new DateTime(endWatch);
   }
   
   /**
    * Sets a specified time to stop watching the given directory or file.
    * Setting an end time which as already expired will stop the watcher immediately.
    * Setting an end time when one was already specified upon construction will override the original.
    * @param endWatch
    * @throws IOException 
    */
   public void stopWatchingNow()
   {
      this.closeQuietly();
   }

   public File getFile() {
	   return file;
   }

   public NotificationMethod getNotificationMethod() {
	   return notificationMethod;
   }

   public String getEndWatchAsString() {
	   if (endWatch == null)
		   return "";
	   return endWatch.toString("MM/dd/yyyy hh:mm a");
   }

   public File getVersionedFileDestinationDir() {
	   return versionedFileDestinationDir;
   }

   public Integer getVersion() {
	   return version;
   }

   public int getPollInterval() {
	   return pollInterval;
   }
}
