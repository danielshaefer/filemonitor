package org.shaeferdp.model.enums;

public enum MonitorType
{
   DIRECTORY, FILE, FILE_VERSIONING;
}
