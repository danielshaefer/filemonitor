package org.shaeferdp.model;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

public class FileCompare
{

   public static String md5(byte[] bytes)
   {
      try
      {
         MessageDigest messageDigest = MessageDigest.getInstance("MD5");
         messageDigest.reset();
         messageDigest.update(bytes);
         byte[] resultByte = messageDigest.digest();
         return String.valueOf(org.apache.commons.codec.binary.Hex.encodeHex(resultByte));
      }
      catch (Exception e)
      {
         throw new RuntimeException(e);
      }
   }
   
   public static String md5(File file)
   {
      try
      {
         return md5(FileUtils.readFileToByteArray(file));
      }
      catch (IOException e)
      {
         throw new RuntimeException(e);
      }
   }
   
   public static boolean areIdentical(File file1, File file2)
   {
      try
      {
         String md5f1 = md5(FileUtils.readFileToByteArray(file1));
         String md5f2 = md5(FileUtils.readFileToByteArray(file2));
         if (StringUtils.equals(md5f1, md5f2))
            return true;
         return false;
      }
      catch (IOException e)
      {
         throw new RuntimeException(e);
      }
   }
   
}
