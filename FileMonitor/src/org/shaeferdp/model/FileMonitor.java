package org.shaeferdp.model;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.joda.time.DateTime;
import org.shaeferdp.model.notification.LoggerNotificationMethod;
import org.shaeferdp.model.poller.FilePollerThread;
import org.shaeferdp.model.poller.FileVersionPollerThread;
import org.shaeferdp.model.poller.PollerThread;
import org.shaeferdp.model.watcher.DirectoryWatcherThread;
import org.shaeferdp.model.watcher.FileVersioningWatcherThread;
import org.shaeferdp.model.watcher.FileWatcherThread;
import org.shaeferdp.model.watcher.WatcherThread;

public class FileMonitor
{
   private Map<File, WatcherThread> mapOfWatchedFiles = new HashMap<File, WatcherThread>();
   private NotificationMethod notificationMethod;
   private Logger logger = Logger.getLogger(FileMonitor.class.getName());
   
   public FileMonitor()
   {
      this.notificationMethod = new LoggerNotificationMethod();
   }
   public FileMonitor(NotificationMethod notificationMethod)
   {
      this.notificationMethod = notificationMethod;
   }
   
   /**
    * Stop watching the file or directory that was started using the given file path. 
    * @param file
    */
   public boolean stopWatchingFile(File file)
   {
      WatcherThread t = mapOfWatchedFiles.get(file);
      if (t != null)
      {
         try
         {
            t.close();
         }
         catch (IOException e)
         {
            e.printStackTrace();
            throw new RuntimeException("Exception closing watch service", e);
         }
         mapOfWatchedFiles.remove(file);
         logger.info("Stopped " + t.getMonitorType() + " watcher using path: " + file);
         return true;
      }
      else
      {
         logger.warning("No watchers were started using the file path: " + file.getAbsoluteFile());
         return false;
      }
   }
   
   public boolean stopWatchingDirectory(File file)
   {
      if (!file.isDirectory())
         file = file.getParentFile();
      return stopWatchingFile(file);
   }
   
   /**
    * Watches a single network file indefinitely. 
    * Network files cannot be watched via watch service so they will be polled.
    * Default Polling Interval is once per day.
    * If a directory is passed in a Runtime Exception will be thrown.
    * @param file (File that is not a directory)
    * @return WatcherThread (This thread return is for convenience. The FileMonitor class handles all basic commands without requiring reference to a given WatcherThread.)
    */
   public WatcherThread startNetworkFileWatch(File file)
   {
      return startNetworkFileWatch(file, null, null);
   }
   
   /**
    * Watches a single network file indefinitely. 
    * Network files cannot be watched via watch service so they will be polled.
    * Default Polling Interval is once per day.
    * If a directory is passed in a Runtime Exception will be thrown.
    * @param file (File that is not a directory)
    * @return WatcherThread (This thread return is for convenience. The FileMonitor class handles all basic commands without requiring reference to a given WatcherThread.)
    */
   public WatcherThread startNetworkFileWatch(File file, Integer pollInterval)
   {
      return startNetworkFileWatch(file, pollInterval, null);
   }
   
   /**
    * Watches a single file until the end time specified by endWatch.
    * If a directory is passed in a Runtime Exception will be thrown.
    * @param file (File that is not a directory)
    * @param pollInterval the interval in millis between poll attempts.
    * @param endWatch when to stop polling for file changes.
    * @return WatcherThread (This thread return is for convenience. The FileMonitor class handles all basic commands without requiring reference to a given WatcherThread.)
    */
   public WatcherThread startNetworkFileWatch(File file, Integer pollInterval, Date endWatch)
   {
      if (!file.getAbsolutePath().startsWith("\\\\"))
      {
         logger.warning("Cannot verify that the file is in a network directory. If it is not, you would be better off using startFileMonitor.");
      }
      PollerThread t = new FilePollerThread(file, notificationMethod, mapOfWatchedFiles, endWatch, pollInterval);
      t.start();
      mapOfWatchedFiles.put(t.getFile(), t);
      return t;
   }
   /**
    * Watches a single network file indefinitely. 
    * Network files cannot be watched via watch service so they will be polled.
    * Default Polling Interval is once per day.
    * If a directory is passed in a Runtime Exception will be thrown.
    * @param file (File that is not a directory)
    * @return WatcherThread (This thread return is for convenience. The FileMonitor class handles all basic commands without requiring reference to a given WatcherThread.)
    */
   public WatcherThread startNetworkFileVersioning(File file, File destFile)
   {
      return startNetworkFileVersioning(file, destFile, null, null);
   }
   
   /**
    * Watches a single network file indefinitely. 
    * Network files cannot be watched via watch service so they will be polled.
    * Default Polling Interval is once per day.
    * If a directory is passed in a Runtime Exception will be thrown.
    * @param file (File that is not a directory)
    * @param destFile (This is the directory where versions of the file will be stored.)
    * @param pollInterval (polling interval in millis-{@link org.joda.time.DateTimeConstants} class is nice for avoiding math)
    * @return WatcherThread (This thread return is for convenience. The FileMonitor class handles all basic commands without requiring reference to a given WatcherThread.)
    */
   public WatcherThread startNetworkFileVersioning(File file, File destFile, Integer pollInterval)
   {
      return startNetworkFileVersioning(file, destFile, pollInterval, null);
   }
   
   /**
    * Watches a single file until the end time specified by endWatch.
    * If a directory is passed in a Runtime Exception will be thrown.
    * @param file (File that is not a directory)
    * @param pollInterval the interval in millis between poll attempts.
    * @param endWatch when to stop polling for file changes.
    * @return WatcherThread (This thread return is for convenience. The FileMonitor class handles all basic commands without requiring reference to a given WatcherThread.)
    */
   public WatcherThread startNetworkFileVersioning(File file, File destFile, Integer pollInterval, Date endWatch)
   {
      if (!file.getAbsolutePath().startsWith("\\\\"))
      {
         logger.warning("Cannot verify that the file is in a network directory. If it is not, you would be better off using startFileMonitor.");
      }
      PollerThread t = new FileVersionPollerThread(file, destFile, notificationMethod, mapOfWatchedFiles, endWatch, pollInterval);
      t.start();
      mapOfWatchedFiles.put(t.getFile(), t);
      return t;
   }
   
   /**
    * Watches a single file indefinitely.
    * If a directory is passed in a Runtime Exception will be thrown.
    * @param file (File that is not a directory)
    * @return WatcherThread (This thread return is for convenience. The FileMonitor class handles all basic commands without requiring reference to a given WatcherThread.)
    */
   public WatcherThread startFileWatch(File file)
   {
      return startFileWatch(file, null);
   }
   
   /**
    * Watches a single file for the duration specified.
    * @param file
    * @param duration Watching will end a number of millis from now based upon duration.
    * @return
    */
   public WatcherThread startFileWatch(File file, long duration)
   {
      return startFileWatch(file, new DateTime().plus(duration).toDate());
   }
   
   /**
    * Watches a single file until the time designated.
    * @param file
    * @param endWatch
    * @return
    */
   public WatcherThread startFileWatch(File file, Date endWatch)
   {
      if (file.isDirectory())
         throw new RuntimeException("Cannot start file watch on a directory.");
      if (file.getAbsolutePath().startsWith("\\\\")) //TODO: Setup more complete way to identify a network path. If we can't we can simply check if the file path given is monitorable in general. If not, we can at least suggest options for the case where it is a network file the user wishes to monitor.
         throw new RuntimeException("Cannot watch a network file please use startNetworkFileWatch.");
      if (mapOfWatchedFiles.containsKey(file))
    	  throw new RuntimeException(file.getName() + " is already being monitored.");
      WatcherThread t = new FileWatcherThread(file, notificationMethod, mapOfWatchedFiles, endWatch);
      t.start();
      mapOfWatchedFiles.put(t.getFile(), t);
      return t;
   }
   
   /**
    * Watch indefinitely for any changes to the directory or files within the given directory.
    * If a file is passed into a directory watch call it will find the parent directory and watch that.
    * @param file
    * @return WatcherThread (This thread return is for convenience. The FileMonitor class handles all basic commands without requiring reference to a given WatcherThread.)
    */
   public WatcherThread startDirectoryWatch(File file) {
      return startDirectoryWatch(file, null);
   }
   
   /**
    * Watch for any changes to the directory or files within the given directory.
    * If a file is passed into a directory watch call it will find the parent directory and watch that.
    * @param file
    * @param duration Watching will end a number of millis from now based upon duration.
    * @return
    */
   public WatcherThread startDirectoryWatch(File file, long duration) {
      return startDirectoryWatch(file, new DateTime().plus(duration).toDate());
   }
   
   /**
    * Watch for any changes to the directory or files within the given directory.
    * If a file is passed into a directory watch call it will find the parent directory and watch that.
    * @param file
    * @param endWatch Watching will end at the Date/Time specified.
    * @return
    */
   public WatcherThread startDirectoryWatch(File file, Date endWatch) {
      WatcherThread t = new DirectoryWatcherThread(file, notificationMethod, mapOfWatchedFiles, endWatch);
      t.start();
      mapOfWatchedFiles.put(t.getFile(), t);
      return t;
   }
   
   public WatcherThread startFileVersioning(File file, File dest, Date endWatch) {
      WatcherThread t = new FileVersioningWatcherThread(file, notificationMethod, mapOfWatchedFiles, dest, endWatch);
      t.start();
      mapOfWatchedFiles.put(t.getFile(), t);
      return t;
   }
   public WatcherThread startFileVersioning(File file, File dest) {
      return startFileVersioning(file, dest, null);
   }
   
   public static WatcherThread monitorFile(File file) {
      FileMonitor monitor = new FileMonitor();
      if (file.getAbsolutePath().startsWith("\\\\"))
         return monitor.startNetworkFileWatch(file);
      else
         return monitor.startFileWatch(file);
   }

   public Map<File, WatcherThread> getMapOfWatchedFiles() {
	   return mapOfWatchedFiles;
   }
}
