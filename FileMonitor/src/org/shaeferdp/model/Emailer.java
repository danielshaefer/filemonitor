package org.shaeferdp.model;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Emailer
{
   private static String MAIL_HOST = "mailserver.com";
   public static void sendMail(String to, String from, String subject, String body)
   {
      sendMail(new String[]{to}, from, subject, body);
   }
   
   public static void sendMail(String[] to, String from, String subject, String body)
   {
      Session session = setupMailSession();
      
      MimeMessage mailMessage = new MimeMessage(session);
      try
      {
         InternetAddress[] addressTo = new InternetAddress[to.length]; 
         for (int i = 0; i < to.length; i++)
         {
            addressTo[i] = new InternetAddress(to[i]);
         }
         mailMessage.setRecipients(Message.RecipientType.TO, addressTo);
         mailMessage.setFrom(new InternetAddress(from));
         mailMessage.setSubject(subject);
         mailMessage.setText(body);
         Transport.send(mailMessage);
      }
      catch(MessagingException ex)
      {
         throw new RuntimeException("Error sending mail", ex);
      }
   }

   private static Session setupMailSession()
   {
      boolean debug = false;
      Properties props = new Properties();
      props.put("mail.smtp.host", MAIL_HOST);
      Session session = Session.getDefaultInstance(props, null);
      session.setDebug(debug);
      return session;
   }
}
