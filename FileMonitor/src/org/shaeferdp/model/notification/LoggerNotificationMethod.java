package org.shaeferdp.model.notification;

import java.io.File;
import java.nio.file.WatchEvent.Kind;
import java.util.logging.Logger;

import org.joda.time.DateTime;
import org.shaeferdp.model.NotificationMethod;
import org.shaeferdp.model.enums.MonitorType;

public class LoggerNotificationMethod extends AbstractNotificationMethod implements NotificationMethod
{
   private static Logger logger = Logger.getLogger(LoggerNotificationMethod.class.getName());
   
   @Override
   public void doNotify(File file, Kind<?> kind, MonitorType monitorType)
   {
      if (file == null)
         throw new RuntimeException("Failed to notify due to null file.");
      String message = getMessage(file, kind, monitorType);
      logger.info(message);
   }

   @Override
   public void doPollNotify(File file, MonitorType monitorType)
   {
      if (file == null)
         throw new RuntimeException("Failed to notify due to null file.");
      String message = getMessage(file, monitorType);
      logger.info(message);
      
   }
}
