package org.shaeferdp.model.notification;

import java.io.File;
import java.nio.file.WatchEvent.Kind;

import org.joda.time.DateTime;
import org.shaeferdp.model.enums.MonitorType;

public abstract class AbstractNotificationMethod
{

   protected String getMessage(File file, Kind<?> kind, MonitorType monitorType)
   {
      return "\n\tWatcher Type: " + monitorType + "\n\tFile Event: " + kind.name() + "\n\tFile Name: " + file.getName() + "\n\tFile Path: " + file.getAbsolutePath() + "\n\tFile last modified: " + new DateTime(file.lastModified()).toString("MM/dd/yyyy hh:mm a");
   }
   
   protected String getMessage(File file, MonitorType monitorType)
   {
      return "\n\tWatcher Type: " + monitorType + "\n\tFile Was Altered" + "\n\tFile Name: " + file.getName() + "\n\tFile Path: " + file.getAbsolutePath() + "\n\tFile last modified: " + new DateTime(file.lastModified()).toString("MM/dd/yyyy hh:mm a");
   }
   
}
