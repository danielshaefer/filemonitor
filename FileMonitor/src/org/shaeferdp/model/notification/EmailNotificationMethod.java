package org.shaeferdp.model.notification;

import java.io.File;
import java.nio.file.WatchEvent.Kind;

import org.shaeferdp.model.Emailer;
import org.shaeferdp.model.NotificationMethod;
import org.shaeferdp.model.enums.MonitorType;

public class EmailNotificationMethod extends AbstractNotificationMethod implements NotificationMethod
{
   
	private String to;

	public EmailNotificationMethod(String to)
	{
		this.to = to;
	}
	
   @Override
   public void doNotify(File file, Kind<?> kind, MonitorType monitorType)
   {
      if (file == null)
         throw new RuntimeException("Failed to notify due to null file.");
      String message = getMessage(file, kind, monitorType);
      Emailer.sendMail(to, "CSMMONITOR@rainhail.com", monitorType + " Watcher detected event: " + kind.name(), message);
   }

   @Override
   public void doPollNotify(File file, MonitorType monitorType)
   {
      if (file == null)
         throw new RuntimeException("Failed to notify due to null file.");
      String message = getMessage(file, monitorType);
      Emailer.sendMail(to, "CSMMONITOR@rainhail.com", monitorType + " Watcher detected file change", message);
   }



}
