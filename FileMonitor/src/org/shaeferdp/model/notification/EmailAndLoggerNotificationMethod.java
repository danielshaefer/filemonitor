package org.shaeferdp.model.notification;

import java.io.File;
import java.nio.file.WatchEvent.Kind;
import java.util.logging.Logger;

import org.shaeferdp.model.Emailer;
import org.shaeferdp.model.NotificationMethod;
import org.shaeferdp.model.enums.MonitorType;

public class EmailAndLoggerNotificationMethod extends AbstractNotificationMethod implements NotificationMethod
{
   
	private static Logger logger = Logger.getLogger(LoggerNotificationMethod.class.getName());
	private String to;
	private String from = "filemonitor@filemonitor.com";

	public EmailAndLoggerNotificationMethod(String to)
	{
		this.to = to;
	}
	public EmailAndLoggerNotificationMethod(String to, String from)
	{
		this.to = to;
		this.from = from;
	}
	
   @Override
   public void doNotify(File file, Kind<?> kind, MonitorType monitorType)
   {
      if (file == null)
         throw new RuntimeException("Failed to notify due to null file.");
      String message = getMessage(file, kind, monitorType);
      logger.info(message);
      Emailer.sendMail(to, from, monitorType + " Watcher detected event: " + kind.name(), message);
   }

   @Override
   public void doPollNotify(File file, MonitorType monitorType)
   {
      if (file == null)
         throw new RuntimeException("Failed to notify due to null file.");
      String message = getMessage(file, monitorType);
      logger.info(message);
      Emailer.sendMail(to, from, monitorType + " Watcher detected file change", message);
   }



}
