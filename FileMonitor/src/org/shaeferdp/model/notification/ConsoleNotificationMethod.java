package org.shaeferdp.model.notification;

import java.io.File;
import java.nio.file.WatchEvent.Kind;

import org.shaeferdp.model.NotificationMethod;
import org.shaeferdp.model.enums.MonitorType;

public class ConsoleNotificationMethod extends AbstractNotificationMethod implements NotificationMethod
{
   @Override
   public void doNotify(File file, Kind<?> kind, MonitorType monitorType)
   {
      if (file == null)
         throw new RuntimeException("Failed to notify due to null file.");
      String message = getMessage(file, kind, monitorType);
      System.out.println(message);
   }

   @Override
   public void doPollNotify(File file, MonitorType monitorType)
   {
      if (file == null)
         throw new RuntimeException("Failed to notify due to null file.");
      String message = getMessage(file, monitorType);
      System.out.println(message);
   }
}
