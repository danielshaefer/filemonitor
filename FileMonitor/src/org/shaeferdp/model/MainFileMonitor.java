package org.shaeferdp.model;

import java.io.File;
import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.shaeferdp.model.notification.EmailAndLoggerNotificationMethod;

public class MainFileMonitor
{
   private static final File file = new File(""); //some file to monitor
   private static final File dirToStoreVersionsOfFile = new File("/");
   public static void main(String[] args) throws IOException
   {
      FileMonitor monitor = new FileMonitor(new EmailAndLoggerNotificationMethod("sampleemail@randomhost.com"));
      
      //DateTime expiration = new DateTime().plusSeconds(30);
      //monitor.startFileVersioning(file, fileDest, expiration.toDate());
      //monitor.startFileWatch(f2);
//      /WatcherThread t = monitor.startFileVersioning(file, fileDest);
//      monitor.startFileWatch(farmOwnersSpecs);
//      monitor.startFileWatch(agriAutoSpecs);
     // monitor.startFileWatch(farmOwnersSpecs);
     //monitor.startNetworkFileMonitor(f2);
     //monitor.startFileVersioning(file, fileDest);
     //monitor.startFileWatch(file);
      //monitor.startFileVersioning(file2, fileDest);
      
      monitor.startNetworkFileVersioning(file, dirToStoreVersionsOfFile, 20000);
     // monitor.startNetworkFileWatch(farmOwnersSpecs, DateTimeConstants.MILLIS_PER_DAY);
     // monitor.startNetworkFileVersioning(agriAutoSpecs, fileDest, DateTimeConstants.MILLIS_PER_DAY);
   }
}
