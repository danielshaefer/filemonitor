package org.shaeferdp.model.poller;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.shaeferdp.model.FileCompare;
import org.shaeferdp.model.enums.MonitorType;
import org.shaeferdp.model.watcher.WatcherThread;

public abstract class PollerThread extends WatcherThread
{
   private static Logger logger = Logger.getLogger(PollerThread.class.getName());
   protected File localFilesDir = new File("C:\\FILE_MONITOR_LOCAL");
   protected int retry = 0;
   protected final int MAX_RETRIES = 10;
   protected final int RETRY_INTERVAL = 2000;
   
   @Override
   public void run()
   {
      try {
         mapOfWatchedFiles.put(file, this);
         while(!closed && !watchIsOver()) {
            File localFile = getLocalCopyOfFile(file);
            if (localFile.exists() && FileCompare.areIdentical(localFile, file))
            {
               logger.info("No changes to file: " + file.getAbsoluteFile());
            }
            else
            {
               writeNewLocalFile(file);
               if (monitorType == MonitorType.FILE_VERSIONING)
               {
                  versionFile();
               }
               notificationMethod.doPollNotify(localFile, monitorType);
            }
            Thread.sleep(pollInterval);
         }
      } catch(InterruptedException ex) {
         ex.printStackTrace();
      }
      finally
      {
         mapOfWatchedFiles.remove(file);
      }
   }

   private void versionFile() throws InterruptedException
   {
      try
      {
         File verInfoFile = new File(versionedFileDestinationDir, ".fminfo");
         Map<String, Integer> versionsByFile = buildVersionMapFromInfoFile(verInfoFile);
         if (verInfoFile.exists())
         {
            //version = NumberUtils.toInt(FileUtils.readFileToString(verInfoFile));
            version = versionsByFile.get(file.getAbsoluteFile().toString());
            if (version == null)
            	version = 0;
         }
         if (version == 0 || !FileCompare.areIdentical(file, getPreviousVersion(file, version)))
         {
            incrementVersionAndWriteNewFile(verInfoFile, versionsByFile);
         }
         else
         {
            logger.warning("Files were identical...did not version: " + file.getAbsolutePath());
         }
      }
      catch(IOException ex)
      {
         retry++;
         Thread.sleep(RETRY_INTERVAL);
         versionFile();
      }
      resetFileWriteRetries();
   }

   private void incrementVersionAndWriteNewFile(File verInfoFile, Map<String, Integer> versionsByFile) throws IOException 
   {
	   version++;
	   versionsByFile.put(file.getAbsoluteFile().toString(), version);
	   FileUtils.writeStringToFile(verInfoFile, convertVersionsMapToString(versionsByFile));
	   FileUtils.copyFile(file, getPreviousVersion(file, version));
   }
   
   private void writeNewLocalFile(File file2) throws InterruptedException
   {
      try
      {
         FileUtils.copyFile(file2, new File(localFilesDir, file2.getName()));
      }
      catch (IOException e)
      {
         e.printStackTrace();
         logger.warning("Was unable to make new local copy of: " + file2.getAbsoluteFile());
         retry++;
         Thread.sleep(RETRY_INTERVAL);
         writeNewLocalFile(file2);
      } 
      resetFileWriteRetries();
   }

   private void resetFileWriteRetries()
   {
      retry = 0;
   }

   private File getLocalCopyOfFile(File file2)
   {
      return new File(localFilesDir, file2.getName());
   }
   
   @Override
   public void close() throws IOException
   {
      closed = true;
   }
   
}
