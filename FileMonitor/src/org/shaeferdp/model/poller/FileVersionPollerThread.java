package org.shaeferdp.model.poller;

import java.io.File;
import java.util.Date;
import java.util.Map;

import org.joda.time.DateTime;
import org.shaeferdp.model.NotificationMethod;
import org.shaeferdp.model.enums.MonitorType;
import org.shaeferdp.model.watcher.WatcherThread;

public class FileVersionPollerThread extends PollerThread
{

   public FileVersionPollerThread(File file, File destFile, NotificationMethod notificationMethod, Map<File, WatcherThread> mapOfWatchedFiles, Integer pollInterval)
   {
      this.monitorType = MonitorType.DIRECTORY;
      this.file = file;
      this.notificationMethod = notificationMethod;
      this.mapOfWatchedFiles = mapOfWatchedFiles;
      this.monitorType = MonitorType.FILE_VERSIONING;
      this.watchSingleFile = true;
      this.versionedFileDestinationDir = destFile;
      if (pollInterval != null && pollInterval >= 1000)
      {
         this.pollInterval = pollInterval;
      }
   }

   public FileVersionPollerThread(File file, File destFile, NotificationMethod notificationMethod, Map<File, WatcherThread> mapOfWatchedFiles, Date endWatch, Integer pollInterval)
   {
      this(file, destFile, notificationMethod, mapOfWatchedFiles, pollInterval);
      if (endWatch != null)
         this.endWatch = new DateTime(endWatch);
   }
   
}
