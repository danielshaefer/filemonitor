package org.shaeferdp.model;

import java.io.File;
import java.nio.file.WatchEvent.Kind;

import org.shaeferdp.model.enums.MonitorType;

public interface NotificationMethod
{
   public void doNotify(File file, Kind<?> kind, MonitorType monitorType);
   public void doPollNotify(File file, MonitorType monitorType);
}
