## Synopsis

FileMonitor is a simple library for watching, monitoring and versioning files. It is built to use the native OS watching built into Java 7 and fallback to polling when the native file watching is not possible (network file watching).

## Motivation

This library was built for two purposes. The first was to tie into the native file watching built into Java 7 purely for academic purposes. The second was because the specs were stored on a shared directory and constantly changed.

## Installation

There is no real installation. While it was never fully made into a library it is designed with that in mind. The class MainFileMonitor is starting point to play with the api.

## API

For file watching you create a monitor which can optionally take a monitor (the default is a Logger monitor which ties into log4j). You can then call a number of static methods focused around watching files or directories. If the file is a local one Java 7 is allowed to do its thing and you will get live "pushed" notification of file changes, deletions, etc. There are several monitor implementations included in the project as well. A type of watching I have called versioning is also available. It works exactly like watching except that you also specify a destination directory and whenever a change occurs the new version of the file is copied to your destination directory.

	:::javascript
	FileMonitor monitor = new FileMonitor();
	monitor.startFileWatch(myFile);